/**
 * @file TestMonitor.cpp
 */
#include "TestMonitor.hpp"
#include <future>

TestMonitor::TestMonitor(std::string name, std::string version,
                         std::vector<gpio::ChipLines>& lines)
           : m_name(name)
           , m_monitor(new gpio::Monitor(version))
           , m_lines(lines)
{
}

void
TestMonitor::threadMain(int msec) const
{
    if (m_monitor->initialise(m_lines))
    {
        std::future<void> f = std::async(std::launch::async,
                              &TestMonitor::terminate, this, msec);
        m_monitor->setCallbackFunc(callback);
        std::cout << m_name << " thread is about to be started!\n";
        m_monitor->start();
        f.get();
    }
}

void
TestMonitor::terminate(int msec) const
{
    std::this_thread::sleep_for(std::chrono::seconds(msec/1000));
    std::cout << m_name << " thread is about to be stopped!\n";
    m_monitor->stop();
}

void
TestMonitor::callback(const char* gpiod, int offset, int value)
{
    std::cout << "chip name: " << gpiod << " line offset: " << offset
              << " line value: " << value << std::endl;
}
