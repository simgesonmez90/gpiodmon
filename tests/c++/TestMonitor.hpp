/**
 * @file TestMonitor.hpp
 * @brief Each monitor would take 30 secs and then be stopped via
 * gpio::Monitor::stop(). 
 */
#include "../../gpiodmon/include/monitor/Monitor.hpp"
#include <iostream>

class TestMonitor
{
public:
    TestMonitor(std::string name, std::string version,
                std::vector<gpio::ChipLines>& lines);
    ~TestMonitor() = default;

    /**
     * @brief Main method on which the thread is executed.
     * @param msec duration in msec that the thread takes.
     */
    void threadMain(int msec) const;
    static void callback(const char* gpiod, int offset, int value);

private: /*  */
    TestMonitor() = delete;
    TestMonitor(TestMonitor const& /* mntr */) = delete;
    TestMonitor& operator=(TestMonitor const& /* mntr */) = delete;

    void terminate(int msec) const;
private:
    std::string                    m_name;
    std::shared_ptr<gpio::Monitor> m_monitor;
    std::vector<gpio::ChipLines>   m_lines;
};
