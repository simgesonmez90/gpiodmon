/**
 * @file main.cpp
 * @brief Implements a multiple-monitor test. Two different monitors  would be
 * instantiated and executed in parallel. First input monitor is executed
 * on DI1,DI4 and the second is on DI2,DI3.
 * To verify that each monitor works as expected, digital inputs should be turned
 * on/off manually.
 * GPIO lines to be monitored:
 * gpiochip0 - pin7 -> DI1
 * gpiochip0 - pin6 -> DI4
 * gpiochip5 - pin20 -> DI2
 * gpiochip5 - pin19 -> DI3
 */

#include "TestMonitor.hpp"
#include <thread>

int main(void)
{
    try
    {
        std::shared_ptr<TestMonitor> monitors[2];
        std::vector<gpio::ChipLines> lines[2] = { {{"gpiochip0", {6}}},
                                                  {{"gpiochip0", {7}}} };

        int duration1 = 20000;      // 20 sec
        monitors[0] = std::make_shared<TestMonitor>("Monitor1", "version1", lines[0]);
        std::thread th_monitor1(&TestMonitor::threadMain, monitors[0],duration1);

        /* wait before starting the second thread */
        std::this_thread::sleep_for(std::chrono::milliseconds(500));

        int duration2 = 30000;      // 30 sec
        monitors[1] = std::make_shared<TestMonitor>("Monitor2", "version2", lines[1]);
        std::thread th_monitor2(&TestMonitor::threadMain, monitors[1], duration2);

        th_monitor1.join();
        th_monitor2.join();
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
    return 0;
}
