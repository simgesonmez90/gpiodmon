using System;
using System.Runtime.InteropServices;

class MonitorBase
{
    [DllImport("libgpiodmon.so", EntryPoint = "create_monitor")]
    public static extern IntPtr createMonitor();

    [DllImport("libgpiodmon.so", EntryPoint = "create_monitor_v2")]
    public static extern IntPtr createMonitorV2();

    [DllImport("libgpiodmon.so", EntryPoint = "destroy_monitor")]
    public static extern void destroyMonitor(IntPtr monitor);


    [DllImport("libgpiodmon.so", EntryPoint = "initialise_monitor")]
    public static extern bool initialise(IntPtr monitor, Lines[] arr, byte size);

    [DllImport("libgpiodmon.so", EntryPoint = "start_monitor")]
    public static extern void start(IntPtr monitor);

    [DllImport("libgpiodmon.so", EntryPoint = "set_monitor_callback")]
    public static extern void setCallback(IntPtr monitor, IntPtr func);

    [DllImport("libgpiodmon.so", EntryPoint = "stop_monitor")]
    public static extern void stop(IntPtr monitor);
}