using System;

public class InputMonitor : IDisposable
{
    public IntPtr Monitor { get; set; }
    public InputMonitor()
    {
        Monitor = MonitorBase.createMonitorV2();
    }

    public bool Initialise(Lines[] lines)
    {
        return MonitorBase.initialise(Monitor, lines, (byte)lines.Length);
    }

    public void Start()
    {
        MonitorBase.start(Monitor);
    }

    public void Stop()
    {
        MonitorBase.stop(Monitor);
    }

    public void SetCallback(IntPtr delegateFunc)
    {
        MonitorBase.setCallback(Monitor, delegateFunc);
    }

    public void Dispose()
    {
        MonitorBase.destroyMonitor(Monitor);
    }
}