using System;
using System.Threading;
using System.Runtime.InteropServices;

class Program
{
    public delegate void MonitorCb(string chip, int offset, int val);
    static void MonitorUpdate(string chip, int offset, int val)
    {
        Console.WriteLine("chip: " + chip + " - offset: " + offset + " - new value: " + val);
    }
    static void Main(string[] args)
    {
        try
        {
            InputMonitor monitor = new InputMonitor();

            Lines[] lines = new Lines[4] {
            new Lines {chip = "gpiochip0", offsets = new byte[]{7}, size = (byte)1},
            new Lines {chip = "gpiochip5", offsets = new byte[]{20}, size = (byte)1},
            new Lines {chip = "gpiochip5", offsets = new byte[]{19}, size = (byte)1},
            new Lines {chip = "gpiochip0", offsets = new byte[]{6}, size = (byte)1} };

            if (monitor.Initialise(lines) == true)
            {
                IntPtr delegateObj = Marshal.GetFunctionPointerForDelegate(new MonitorCb(MonitorUpdate));
                monitor.SetCallback(delegateObj);
                monitor.Start();
            }
            else
            {
                monitor.Dispose();
                throw new Exception("Error: Monitor not initialised successfully!");
            }

        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
}
