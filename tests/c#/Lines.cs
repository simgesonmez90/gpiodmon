using System;
using System.Runtime.InteropServices;

[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
public struct Lines
{
    public string chip;
    public byte[] offsets;
    public byte size;
}