/**
 * @file  gpiodmon_wrap.h C Wrapper API
 * @brief This wrapper wraps gpio::Monitor C++ API so that
 * user of gpiodmon is allowed to access native unmanaged
 * code from managed code using P/Invoke API. It is user's
 * responsibility to initialise Monitor first with correct
 * chip name & offset values before starting it. User must
 * stop monitor in another thread as monitoring blocks the
 * thread in which it is executed.
 */
#ifndef __CROSS_GPIODMON_WRAP_H__
#define __CROSS_GPIODMON_WRAP_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @struct chip_lines
 * @brief This structure holds the chip name & offsets pair.
 * @property chip name record of the chip, expected to be a
 * string like "gpiochip0", "gpiochip5" etc.
 * @property offsets offset values of the chip, valid numbers
 * must be smaller than the number of lines which belongs to
 * the chip. It might be in the range both between [0, 32) or
 * between [0, 64) depending on the chip.
 * @property size the size of the offsets array.
 */
typedef struct chip_lines
{
    const char* chip;
    unsigned char* offsets;
    unsigned char  size;
} chip_lines;

/**
 * @brief Create an input monitor version 1 instance.
 * @return void pointer to gpio::Monitor object.
 */
void* create_monitor();

/**
 * @brief Create an input monitor version 2 instance.
 * @return void pointer to gpio::Monitor object.
 */
void* create_monitor_v2();

/**
 * @brief Remove input monitor from the monitors list.
 * @param object void pointer to gpio::Monitor object to be removed.
 */
void destroy_monitor(void* object);

/**
 * @brief Initilise the input monitor with the parameters given.
 * @param object pointer to gpio::Monitor to be initialised.
 * @param lines an array of chip&lines pair to be monitored.
 * In the case that chip name or offset number is invalid, this
 * throws an exception containing the error information.
 * @param size size of the chip_lines array.
 * @return true if monitor is initialised successfully,false o/w.
 */
bool initialise_monitor(void* object,
                        chip_lines* lines,
                        unsigned char size);

/**
 * @brief Start monitoring the lines pre-configured.It resets
 * configuration & stops to monitor & throws an exception with
 * detailed info if any problem occurs. Since this function is
 * blocking, monitoring must be closed in another thread using
 * stopMonitor(). User must set the lines which s/he wants to
 * monitor before starting it.
 * @param object pointer to gpio::Monitor to be started.
 */
void start_monitor(void* object);

/**
 * @brief Register callback for event monitoring. This callback
 * shall be fired with the required information in the case that
 * any change happens on the lines which are being monitored at
 * the moment.
 * @param object  pointer to gpio::Monitor for which callback is
 * registered.
 * @param callback callback function to be populated by client.
 * Callback function is like:  void (*Cb)(const char*, int, int)
 * where the first parameter is the name of the chip, the second
 * parameter is the offset number and the final parameter is the
 * new value of the line.
 */
void set_monitor_callback(void* object,void* callback);

/**
 * @brief Terminate monitoring. This function shall be called in
 * different thread from the thread in which monitor is executed
 * since monitroing is blocking.
 * It clears & resets configuration when monitoring is stopped.
 * @param object  pointer to gpio::Monitor to be stopped.
 */
void stop_monitor(void* object);

#ifdef __cplusplus
}
#endif

#endif /* __CROSS_GPIODMON_WRAP_H__ */
