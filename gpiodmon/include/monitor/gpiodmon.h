/**
 * @file  gpiodmon.h C API
 * @brief This is a C API which makes available for the users to read
 * the values of multiple IO lines which belong to different gpiochips.
 * It notifies the users of the values via callbacks registered before.
 * Each user would have it's own handle and thus own callbacks waiting
 * to get triggered.
 */
#ifndef __CROSS_GPIODMON_H__
#define __CROSS_GPIODMON_H__
#include <gpiod.h>

#ifdef __cplusplus
extern "C" {
#endif
#define MAX_NUM_CHIPS 9

typedef void (*cross_gpiod_monitor_cb)(int, unsigned int, void*);
struct cross_gpiod_cb_user_pair
{
    cross_gpiod_monitor_cb callback;
    void* user_data;
};

typedef struct cross_gpiod_monitor_handle cross_gpiod_monitor_handle;

/**
 * @brief Register callback to be kept notified of the input values.
 * @param handle the monitor handle for which callback is registered.
 * @param cb callback to be registered for the events.
 * @param user_data pointer to the object which will consume callback
 * data.
 */
void cross_gpiod_register_monitor_cb(cross_gpiod_monitor_handle* handle,
                                     cross_gpiod_monitor_cb cb,
                                     void* user_data);

/**
 * @brief Create and initialise a monitor with default values.
 * @return Returns a pointer to the monitor handle.
 */
cross_gpiod_monitor_handle* cross_gpiod_initialise_monitor();

/**
 * @brief Destroy the monitor given.
 * @param A pointer to the monitor handle.
 */
void cross_gpiod_destroy_monitor(cross_gpiod_monitor_handle* handle);

/**
 * @brief Configure monitoring. Set how much file descriptors
 * will being monitored and the timeout in miliseconds.
 * @param handle the monitor handle to be configured.
 * @param file_descs file descriptor array to be monitored.
 * @param num_files number of the file descriptors.
 * in milliseconds which monitoring blocks the thread.
 * @param timeout time duration that specifies the interval
 * in milliseconds which monitoring blocks the thread.
 */
void cross_gpiod_configure_monitor(cross_gpiod_monitor_handle* handle,
                                   int* file_descs,
                                   int num_files, int timeout);

/**
 * @brief Start to monitor gpiod lines.
 * @param handle pointer to the monitor handle to be monitored.
 * @return -1 on failure, 0 if timeout expires, 1 on success.
 * 1 means that monitor is started successfully and blocking
 * is removed by some events on the file descriptors being
 * monitored.
 */
int cross_gpiod_run_monitor(cross_gpiod_monitor_handle* handle);
int cross_gpiod_run_monitor_v2(cross_gpiod_monitor_handle* handle);

#ifdef __cplusplus
}
#endif /* extern "C" */
#endif /* __CROSS_GPIODMON_H__ */
