/**
 * @file  DeviceManager.hpp
 * @brief DeviceManager holds and manages list of the devices to
 * be monitored.
 */
#ifndef __CROSS_GPIO_DEVICEMANAGER_HPP__
#define __CROSS_GPIO_DEVICEMANAGER_HPP__

#include <memory>
#include <vector>

namespace gpio
{
class MonitorIF;
class Device;
class DeviceManager
{
public:
    DeviceManager(std::shared_ptr<MonitorIF>& monitor_if);
    ~DeviceManager();

    bool initialise(std::vector<std::tuple<std::string,
                    std::vector<unsigned int>>>& lines) noexcept(false);

    /**
     * @brief Get devices.
     * @return with the list of existing devices. It would be
     * empty if no device is found.
     */
    void getDevices(std::vector<std::shared_ptr<Device>>& devs) const;

    /**
     * @brief Remove all devices from the list.
     */
    void clearDevices();
private:
    DeviceManager() = delete;
    DeviceManager(DeviceManager const& /*mngr*/) = delete;
    DeviceManager& operator=(DeviceManager const& /*mngr*/) = delete;

private:
    std::shared_ptr<MonitorIF>           m_monitor_if;
    std::vector<std::shared_ptr<Device>> m_devices;
};
}  // namespace gpio
#endif /* __CROSS_GPIO_DEVICEMANAGER_HPP__*/
