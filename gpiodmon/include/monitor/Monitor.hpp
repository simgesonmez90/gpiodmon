/**
 * @file Monitor.hpp
 * @brief C++ API that makes monitoring the GPIOs available to
 * the users. Since monitoring itself is blocking, it would be
 * user's responsibility to stop it in another thread.
 */
#ifndef __CROSS_GPIO_MONITOR_HPP__
#define __CROSS_GPIO_MONITOR_HPP__

#include "AnyObserver.inl"
#include "Device.hpp"

/* Callback Function
 * This callback returns name and offset of the device which
 * event belongs to. It also returns the new value of the GPIO.
 */
typedef void (*MonitorCb)(const char*, int, int);
namespace gpio
{
class MonitorIF;
class DeviceManager;
struct ChipLines
{
    std::string chip_name;
    std::vector<unsigned int> offsets;
};

class Monitor : public AnyObserver<Device, cross_gpiod_cb_data>,
                public std::enable_shared_from_this<Monitor>
{
public:
    Monitor(std::string const& version = "version1") noexcept(false);
    virtual ~Monitor();

    /**
     * @brief Initialise monitor with the lines to be monitored.
     */
    bool initialise(std::vector<ChipLines> const& lines) noexcept(false);

    /**
     * @brief Set callback function. This callback will be fired when
     * rising/falling edge events happen.
     */
    void setCallbackFunc(MonitorCb callback) noexcept;

    /**
     * @brief Start to monitor gpiod lines registered before. Caller is
     * responsible for registering the gpiod lines to be monitored before
     * starting monitor. As this is blocking, caller should stop monitor
     * in another thread using stop() when the need arises.
     */
    void start() noexcept(false);

    /**
     * @brief Stop monitoring gpiod lines. Caller is responsible to start
     * monitor before stopping it. This would do nothing if there is no
     * monitor running.
     */
    void stop() noexcept;

    // Implementing AnyObserver
    void update(Device* chip, cross_gpiod_cb_data& info) const override;

private:
    Monitor(Monitor const& /*monitor*/) = delete;
    Monitor& operator=(Monitor const& /*monitor*/) = delete;

    bool registerLines(std::vector<ChipLines> const& lines) const;

private:
    std::shared_ptr<MonitorIF>          m_monitor_if;
    std::unique_ptr<DeviceManager>      m_device_manager;
    MonitorCb                           m_callback;
};
}
#endif /* __CROSS_GPIO_MONITOR_HPP__*/
