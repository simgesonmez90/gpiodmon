/**
 * @file MonitorIF.hpp
 * @brief Interface to C API gpiodmon.h. MonitorIF holds gpiod file
 * descriptors to be monitored and starts/stops the monitor via these
 * file descriptors.
 */
#ifndef __CROSS_GPIO_MONITORIF_HPP__
#define __CROSS_GPIO_MONITORIF_HPP__
#include "gpiodmon.h"
#include <future>
#include <string>
#include <vector>

namespace gpio
{
class MonitorIF
{
public:
    MonitorIF(std::string const& version);
    ~MonitorIF();

    void addFileDescriptor(int const fd);
    void start() noexcept(false);
    void stop();
    cross_gpiod_monitor_handle* handle() const;
    std::string getVersion() const;

private:
    MonitorIF(MonitorIF const& /*monitor_if*/) = delete;
    MonitorIF& operator=(MonitorIF const& /*monitor_if*/) = delete;

    void run(std::function<int(cross_gpiod_monitor_handle*)>& func);
    void resetMonitor();
private:
    const std::string                m_version;
    cross_gpiod_monitor_handle*      m_handle;
    std::promise<bool>               m_stopped;
    std::future<bool>                m_done;
    std::vector<int>                 m_file_descriptors;
};
}  // namespace gpio
#endif /* __CROSS_GPIO_MONITORIF_HPP__ */
