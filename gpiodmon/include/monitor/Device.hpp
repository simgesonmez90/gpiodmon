/**
 * @file  Device.hpp
 * @brief Device gets and holds the gpio lines to be monitored.
 * Observers would be notified of the rising & falling edge events
 * if any input value changes as this class is also a publisher.
 */
#ifndef __CROSS_GPIO_DEVICE_HPP__
#define __CROSS_GPIO_DEVICE_HPP__

#include "gpiodmon.h"
#include "MonitorIF.hpp"
#include "AnySubject.inl"

namespace gpio
{
typedef std::tuple<unsigned int, unsigned int> cross_gpiod_cb_data;
class Device : public AnySubject<Device, cross_gpiod_cb_data>
{
public:
    Device(std::shared_ptr<MonitorIF>& monitor_if, std::string& name,
           std::vector<unsigned int>& offsets) noexcept(false);
    virtual ~Device();

    std::string getName() const;

    friend void monitorCb(int fd, unsigned int value, void* user_data);
private:
    Device() = delete;
    Device(Device const& /*device*/) = delete;
    Device& operator=(Device const& /*device*/) = delete;

private:
    struct GpiodChip;
    std::shared_ptr<MonitorIF>        m_monitor_if;
    std::unique_ptr<GpiodChip>        m_chip;
    const std::string                 m_name;
};
}  // namespace gpio
#endif /* __CROSS_GPIO_DEVICE_HPP__ */
