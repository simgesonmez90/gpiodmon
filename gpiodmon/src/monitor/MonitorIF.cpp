/**
 * @file MonitorIF.cpp
 */
#include "MonitorIF.hpp"

#include <algorithm>

namespace gpio
{
MonitorIF::MonitorIF(std::string const& version)
         : m_version(version)
         , m_handle(nullptr)
         , m_done(m_stopped.get_future())
{
    m_handle = cross_gpiod_initialise_monitor();
}

MonitorIF::~MonitorIF()
{
    resetMonitor();
    cross_gpiod_destroy_monitor(m_handle);
}

void
MonitorIF::addFileDescriptor(int const fd)
{
    if (fd == -1)
    {
        throw std::invalid_argument("invalid fd: " + std::to_string(fd));
    }

    auto found = std::find_if(m_file_descriptors.begin(),
                              m_file_descriptors.end(),
                              [&fd](auto file_descriptor)->bool
                              {
                                  return fd == file_descriptor;
                              });
    if (found == m_file_descriptors.end())
    {
        m_file_descriptors.emplace_back(fd);
    }
}

void
MonitorIF::start() noexcept(false)
{
    m_stopped = std::promise<bool>();
    m_done = m_stopped.get_future();
    std::function<int(cross_gpiod_monitor_handle*)> func;

    int timeout = 500;
    func = cross_gpiod_run_monitor;
    if (m_version.compare("version2") == 0)
    {
        timeout = 0;
        func = cross_gpiod_run_monitor_v2;
    }

    cross_gpiod_configure_monitor(m_handle, m_file_descriptors.data(),
                                  m_file_descriptors.size(), timeout);
    try
    {
        run(func);
    }
    catch (const std::runtime_error& re)
    {
        resetMonitor();
        throw;
    }
}

void
MonitorIF::run(std::function<int(cross_gpiod_monitor_handle*)>& func)
{
    std::future_status status = std::future_status::deferred;
    while (status != std::future_status::ready)
    {
        if (func(m_handle) == -1)
        {
            throw std::runtime_error("Monitor has failed to run!");
        }
        status = m_done.wait_for(std::chrono::milliseconds(10));
    }

    if (m_done.get() == true)
    {
        resetMonitor();
    }
}

void
MonitorIF::resetMonitor()
{
    m_file_descriptors.clear();
    cross_gpiod_configure_monitor(m_handle, nullptr, 0, 0);
}

void
MonitorIF::stop()
{
    m_stopped.set_value(true);
}

cross_gpiod_monitor_handle*
MonitorIF::handle() const
{
    return m_handle;
}

std::string
MonitorIF::getVersion() const
{
    return m_version;
}
}  // namespace gpio
