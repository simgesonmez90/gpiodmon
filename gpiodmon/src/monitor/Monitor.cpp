/**
 * @file Monitor.cpp
 */
#include "Monitor.hpp"

#include "MonitorIF.hpp"
#include "DeviceManager.hpp"

namespace gpio
{
Monitor::Monitor(std::string const& version)
       : m_monitor_if(new MonitorIF(version))
       , m_device_manager(new DeviceManager(m_monitor_if))
{
}

Monitor::~Monitor()
{
}

bool
Monitor::initialise(std::vector<ChipLines> const& lines) noexcept(false)
{
    if (lines.empty())
    {
        throw std::invalid_argument("No line exists in the list!");
    }

    std::vector<std::tuple<std::string, std::vector<unsigned int>>> chip_lines;
    for (auto const& line : lines)
    {
        auto found = std::find_if(chip_lines.begin(), chip_lines.end(),
                [&line](auto const& chip_line)
                {
                    return std::get<std::string>(chip_line) == line.chip_name;
                });
        if (found == chip_lines.end())
        {
            chip_lines.emplace_back(std::make_tuple(line.chip_name, line.offsets));
        }
        else
        {
            auto& offsets = std::get<std::vector<unsigned int>>(*found);
            offsets.insert(offsets.end(), line.offsets.begin(), line.offsets.end());
        }
    }
    return m_device_manager->initialise(chip_lines);
}

void
Monitor::setCallbackFunc(MonitorCb callback) noexcept
{
    m_callback = callback;
}

void
Monitor::start() noexcept(false)
{
    std::vector<std::shared_ptr<Device>> devices;
    m_device_manager->getDevices(devices);

    for (auto& device : devices)
    {
        device->attach(shared_from_this());
    }
    m_monitor_if->start();
}

void
Monitor::stop() noexcept
{
    m_device_manager->clearDevices();
    m_monitor_if->stop();
}

void
Monitor::update(Device* chip, cross_gpiod_cb_data& info) const
{
    if (chip != nullptr && m_callback)
    {
        std::string name(chip->getName());
        m_callback(name.c_str(), std::get<0>(info), std::get<1>(info));
    }
}
}
