/**
 * @file DeviceManager.cpp
 */
#include "DeviceManager.hpp"
#include "MonitorIF.hpp"
#include "Device.hpp"

namespace gpio
{
DeviceManager::DeviceManager(std::shared_ptr<MonitorIF>& monitor_if)
             : m_monitor_if(monitor_if)
{
}

DeviceManager::~DeviceManager()
{
}

bool
DeviceManager::initialise(std::vector<std::tuple<std::string,
                          std::vector<unsigned int>>>& lines) noexcept(false)
{
    std::for_each(lines.begin(), lines.end(), [&](auto const& line)
    {
        auto chip_name = std::get<std::string>(line);
        auto offsets = std::get<std::vector<unsigned int>>(line);
        std::shared_ptr<Device> dev{ new Device(m_monitor_if, chip_name, offsets) };
        m_devices.emplace_back(std::move(dev));
    });
    return lines.size() == m_devices.size();
}

void
DeviceManager::getDevices(std::vector<std::shared_ptr<Device>>& devs) const
{
    devs = m_devices;
}

void
DeviceManager::clearDevices()
{
    m_devices.clear();
}
}  // namespace gpio
