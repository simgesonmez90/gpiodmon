/**
 * @file Device.cpp
 */
#include "Device.hpp"
#include "MonitorIF.hpp"
#include <linux/gpio.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>

#define GET_VALUE GPIOHANDLE_GET_LINE_VALUES_IOCTL
namespace gpio
{
struct Device::GpiodChip
{
    struct gpiod_chip* _chip;
    GpiodChip() = delete;
    GpiodChip(std::string chip_name)
      : _chip(gpiod_chip_open_by_name(chip_name.c_str())) {
        if (_chip == nullptr)
            throw std::invalid_argument("GPIO device could not be found!");
    }
    ~GpiodChip() {
        m_lines.clear();
        gpiod_chip_close(_chip);
    }

    struct GpiodLine;
    std::vector<std::unique_ptr<struct GpiodLine>> m_lines;
    int addLine(unsigned int offset, std::string version);
};

struct Device::GpiodChip::GpiodLine
{
    std::string _version;
    unsigned int _offset;
    struct gpiod_line* _ptr;
    unsigned int _value;
    int _fd;

    GpiodLine() = delete;
    GpiodLine(struct gpiod_chip* chip, unsigned int offset, std::string version)
      : _version(version)
      , _offset(offset)
      , _value(0xFF)
      , _fd(-1) {
        if (offset >= gpiod_chip_num_lines(chip))
            throw std::out_of_range("invalid offset: " + std::to_string(offset));
        _ptr = gpiod_chip_get_line(chip, _offset);;
    }
    ~GpiodLine() {
        if (_version.compare("version2") == 0) { if (_fd != -1) close(_fd); }
    }
    void initialise(std::string chip_name);
    int requestInput(int chip_fd) const;
};

int
Device::GpiodChip::addLine(unsigned int offset, std::string version)
{
    std::unique_ptr<GpiodLine> line(new GpiodLine(_chip, offset, version));
    line->initialise(std::string(gpiod_chip_name(_chip)));
    int file_desc = line->_fd;
    m_lines.emplace_back(std::move(line));
    return file_desc;
}

void
Device::GpiodChip::GpiodLine::initialise(std::string chip_name)
{
    if (_version.compare("version2") == 0)
    {
        char *path;
        asprintf(&path, "/dev/%s", chip_name.c_str());
        int chip_fd = open(path, O_RDONLY | O_NONBLOCK);

        struct gpiohandle_data data;
        _fd = requestInput(chip_fd);
        _value = ioctl(_fd, GET_VALUE, &data) < 0 ? -1 : data.values[0];

        close(chip_fd);
        free(path);
    }
    else
    {
        gpiod_line_request_both_edges_events(_ptr, "");
        _fd = gpiod_line_event_get_fd(_ptr);
        _value = gpiod_line_get_value(_ptr);
    }
}

int
Device::GpiodChip::GpiodLine::requestInput(int chip_fd) const
{
    struct gpiohandle_request request;
    request.lines = 1;
    request.lineoffsets[0] = _offset;
    request.flags = GPIOHANDLE_REQUEST_INPUT;
    if (ioctl(chip_fd, GPIO_GET_LINEHANDLE_IOCTL, &request) == -1)
    {
        return -1;
    }
    return request.fd;
}

inline void monitorCb(int fd, unsigned int value, void* user_data)
{
    Device* device = static_cast<Device*>(user_data);
    auto exists = std::find_if(device->m_chip->m_lines.begin(),
                               device->m_chip->m_lines.end(),
                               [&fd](auto const& line)
                               { 
                                   return fd == line->_fd;
                               });

    if (exists != device->m_chip->m_lines.end() && (*exists)->_value != value)
    {
        (*exists)->_value = value;
        auto info = std::make_tuple((*exists)->_offset, (*exists)->_value);
        device->notify(device, info);
    }
}

Device::Device(std::shared_ptr<MonitorIF>& monitor_if, std::string& name,
               std::vector<unsigned int>& offsets) noexcept(false)
       : m_monitor_if(monitor_if)
       , m_name(name)
{
    m_chip = std::make_unique<GpiodChip>(m_name);
    std::for_each(offsets.begin(), offsets.end(), [&](auto const& offset)
    {
        int fd = m_chip->addLine(offset, m_monitor_if->getVersion());
        m_monitor_if->addFileDescriptor(fd);
    });
    cross_gpiod_register_monitor_cb(m_monitor_if->handle(),monitorCb, this);
}

Device::~Device()
{
}

std::string
Device::getName() const
{
    return m_name;
}
}  // namespace gpio
