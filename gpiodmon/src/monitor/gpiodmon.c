/**
 * @file gpiodmon.c
 */
#include "gpiodmon.h"
#include <linux/gpio.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

struct cross_gpiod_monitor_handle
{
    struct timeval expire;
    fd_set fds_set;
    int fds[FD_SETSIZE];
    int fds_size;
    /* callbacks of the monitor which belong to the different chips */
    struct cross_gpiod_cb_user_pair cb_user_pairs[MAX_NUM_CHIPS];
    unsigned int cb_index;
};

void cross_gpiod_register_monitor_cb(cross_gpiod_monitor_handle* handle,
                                     cross_gpiod_monitor_cb cb,
                                     void* user_data)
{
    if (handle == NULL || handle->cb_index == MAX_NUM_CHIPS)
    {
        return;
    }
    handle->cb_user_pairs[handle->cb_index].callback = cb;
    handle->cb_user_pairs[handle->cb_index].user_data = user_data;
    handle->cb_index++;
}

cross_gpiod_monitor_handle* cross_gpiod_initialise_monitor()
{
    cross_gpiod_monitor_handle* monitor_handle =
      (cross_gpiod_monitor_handle*)malloc(sizeof(struct cross_gpiod_monitor_handle));
    (monitor_handle->expire).tv_sec = 0;
    (monitor_handle->expire).tv_usec = 0;
    FD_ZERO(&(monitor_handle->fds_set));
    memset(monitor_handle->fds, -1, sizeof(monitor_handle->fds));
    monitor_handle->fds_size = 0;
    monitor_handle->cb_index = 0;
    return monitor_handle;
}

void cross_gpiod_destroy_monitor(cross_gpiod_monitor_handle* handle)
{
    if (handle == NULL)
    {
        return;
    };
    free(handle);
    handle = NULL;
}

void cross_gpiod_configure_monitor(cross_gpiod_monitor_handle* handle,
                                   int* file_descs,
                                   int num_files, int timeout)
{
    if (handle == NULL)
    {
        return;
    }

    (handle->expire).tv_sec = timeout/1000;
    (handle->expire).tv_usec = (timeout%1000)*1000;
    if (file_descs == NULL || num_files == 0)
    {
        FD_ZERO(&(handle->fds_set));
        memset(handle->fds, -1, sizeof(handle->fds));
        handle->fds_size = 0;
        handle->cb_index = 0;
        return;
    }

    for (int index=0; index<num_files; index++)
    {
        FD_SET(file_descs[index], &(handle->fds_set));
        handle->fds[handle->fds_size] = file_descs[index];
        handle->fds_size++;
    }
}

int cross_gpiod_run_monitor(cross_gpiod_monitor_handle* handle)
{
    if (handle == NULL)
    {
        return -1;
    }

    fd_set read_fds = handle->fds_set;
    struct timeval to = {(handle->expire).tv_sec, (handle->expire).tv_usec};
    int ret = select(FD_SETSIZE, &read_fds, NULL, NULL, &to);
    if (ret == -1 || ret == 0)
    {
        return ret;
    }

    ret = 1;
    for (int i=0; i<handle->fds_size; ++i)
    {
        if (FD_ISSET(handle->fds[i], &read_fds))
        {
            struct gpiod_line_event event;
            int value = -1;
            ret += gpiod_line_event_read_fd(handle->fds[i], &event);
            for (unsigned int j=0; j<(handle->cb_index); ++j)
            {
                if (event.event_type == GPIOD_LINE_EVENT_RISING_EDGE)
                {
                    value = 1;
                }
                else if (event.event_type == GPIOD_LINE_EVENT_FALLING_EDGE)
                {
                    value = 0;
                }
                (handle->cb_user_pairs[j].callback)(handle->fds[i],
                               value, handle->cb_user_pairs[j].user_data);
            }
        }
    }
    return ret;
}

int cross_gpiod_run_monitor_v2(cross_gpiod_monitor_handle* handle)
{
    if (handle == NULL)
    {
        return -1;
    }

    for (int i=0; i<handle->fds_size; ++i)
    {
        struct gpiohandle_data data;
        if (0 <= ioctl(handle->fds[i], GPIOHANDLE_GET_LINE_VALUES_IOCTL, &data))
        {
            for (unsigned int j=0; j<(handle->cb_index); ++j)
            {
                (handle->cb_user_pairs[j].callback)(handle->fds[i],
                            data.values[0], handle->cb_user_pairs[j].user_data);
            }
        }
    }
    return 1;
}
#ifdef __cplusplus
}
#endif /* extern "C" */
