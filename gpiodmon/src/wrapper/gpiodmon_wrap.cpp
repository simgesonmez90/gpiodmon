/**
 * @file gpiodmon_wrap.cpp
 * @brief This implements gpiodmon_wrap C Wrapper.
 */
#include "gpiodmon_wrap.h"
#include "Monitor.hpp"

static std::vector<std::shared_ptr<gpio::Monitor>> monitors;

static bool find_monitor(void const* obj, std::shared_ptr<gpio::Monitor>& mon)
{
    auto exists = std::find_if(monitors.begin(), monitors.end(),
                              [&obj](auto const& monitor)
                              {
                                  return obj == monitor.get();
                              });
    if (exists == monitors.end())
    {
        return false;
    }
    mon = *exists;
    return true;
}

void* create_monitor()
{
    auto monitor = std::make_shared<gpio::Monitor>("version1");
    monitors.emplace_back(monitor);
    return monitor.get();
}

void* create_monitor_v2()
{
    auto monitor = std::make_shared<gpio::Monitor>("version2");
    monitors.emplace_back(monitor);
    return monitor.get();
}

void destroy_monitor(void* object)
{
    monitors.erase(std::remove_if(monitors.begin(), monitors.end(),
                                  [&object](auto const& monitor)
                                  {
                                      return object == monitor.get();
                                  }),monitors.end());
}

bool initialise_monitor(void* object, chip_lines* lines, unsigned char size)
{
    std::shared_ptr<gpio::Monitor> gpio_monitor;
    if (find_monitor(object, gpio_monitor) == false)
    {
        return false;
    }

    std::vector<gpio::ChipLines> gpio_lines;
    for (int i=0; i<(int)size; i++)
    {
        std::vector<unsigned int> offsets;
        std::string chip_name { lines[i].chip };
        for (int j=0; j<(int)lines[i].size; j++)
        {
            offsets.emplace_back((unsigned int)lines[i].offsets[j]);
        }
        gpio_lines.emplace_back(gpio::ChipLines({ chip_name, offsets }));
    }
    return gpio_monitor->initialise(gpio_lines);
}

void start_monitor(void* object)
{
    std::shared_ptr<gpio::Monitor> gpio_monitor;
    if (find_monitor(object, gpio_monitor) == false)
    {
        return;
    }

    gpio_monitor->start();
}

void set_monitor_callback(void* object, void* callback)
{
    std::shared_ptr<gpio::Monitor> gpio_monitor;
    if (find_monitor(object, gpio_monitor) == false)
    {
        return;
    }

    gpio_monitor->setCallbackFunc((void(*)(const char*, int, int))callback);
}

void stop_monitor(void* object)
{
    std::shared_ptr<gpio::Monitor> gpio_monitor;
    if (find_monitor(object, gpio_monitor) == false)
    {
        return;
    }

    gpio_monitor->stop();
}
