/*
 * @file gpiodmon_test.cpp
 */
#include "gpiodmon_wrap.h"

#include <gtest/gtest.h>
#include <thread>
#include <functional>
#include <future>

static void* mon = nullptr;
TEST(Monitor, Create)
{
    mon = create_monitor_v2();
    ASSERT_NE(nullptr, mon);
}

TEST(Monitor, destroy)
{
    ASSERT_NO_THROW(destroy_monitor(mon));
}

TEST(Monitor, MultipleCreation)
{
    void* object1 = nullptr;
    void* object2 = nullptr;
    object1 = create_monitor_v2();
    ASSERT_NE(nullptr, object1);
    object2 = create_monitor_v2();
    ASSERT_NE(nullptr, object2);
    destroy_monitor(object1);
    destroy_monitor(object2);
}

class MonitorTestFixture : public testing::Test
{
public:
    MonitorTestFixture() : monitor(nullptr) {};
    virtual ~MonitorTestFixture() = default;
    void SetUp() override;
    void TearDown() override;
protected:
    void* monitor;
};

void MonitorTestFixture::SetUp()
{
    monitor = create_monitor_v2();
}

void MonitorTestFixture::TearDown()
{
    destroy_monitor(monitor);
}

TEST_F(MonitorTestFixture, Initialise)
{
    /* arrange */
    const char* chip = "gpiochip0";
    unsigned char offsets[2] = {(unsigned char)(6), (unsigned char)(7)};
    chip_lines lines[1] = { {chip, offsets, (unsigned char)(2)} };
    unsigned char size = (unsigned char)(1);

    /* act and assert */
    EXPECT_TRUE(initialise_monitor(monitor, lines, size));
}

TEST_F(MonitorTestFixture, InitialiseWith4Args)
{
    /* arrange */
    const char* chip1 = "gpiochip0";
    const char* chip2 = "gpiochip5";
    unsigned char offset1[1] = {(unsigned char)(7)};
    unsigned char offset2[1] = {(unsigned char)(20)};
    unsigned char offset3[1] = {(unsigned char)(19)};
    unsigned char offset4[1] = {(unsigned char)(6)};
    chip_lines lines[4] = { {chip1, offset1, (unsigned char)(1)},
                            {chip2, offset2, (unsigned char)(1)},
                            {chip2, offset3, (unsigned char)(1)},
                            {chip1, offset4, (unsigned char)(1)} };
    unsigned char size = (unsigned char)(4);

    /* act and assert */
    EXPECT_TRUE(initialise_monitor(monitor, lines, size));
}

TEST_F(MonitorTestFixture, InitialiseWithNoLine)
{
    /* arrange */
    chip_lines* lines = nullptr;
    unsigned char size = (unsigned char)(0);

    /* act and assert */
    EXPECT_THROW(initialise_monitor(monitor, lines, size), std::invalid_argument);
}

TEST_F(MonitorTestFixture, InitialiseWithInvalidDevice)
{
    /* arrange */
    const char* chip = "gpiochipx";
    unsigned char offsets[2] = {(unsigned char)(6), (unsigned char)(7)};
    chip_lines lines[1] = {chip, offsets, (unsigned char)(2)};
    unsigned char size = (unsigned char)(1);

    /* act and assert */
    EXPECT_THROW(initialise_monitor(monitor, lines, size), std::invalid_argument);
}

TEST_F(MonitorTestFixture, InitialiseWithSameLineTwice)
{
    /* arrange */
    const char* chip = "gpiochip5";
    unsigned char offsets[2] = {(unsigned char)(19), (unsigned char)(19)};
    chip_lines lines[1] = {chip, offsets, (unsigned char)(2)};
    unsigned char size = (unsigned char)(1);

    /* act and assert */
    EXPECT_THROW(initialise_monitor(monitor, lines, size), std::invalid_argument);
}

TEST_F(MonitorTestFixture, InitialiseWithInvalidLine)
{
    /* arrange */
    const char* chip = "gpiochip0";
    unsigned char offsets[1] = {(unsigned char)(100)};
    chip_lines lines[1] = {chip, offsets, (unsigned char)(1)};
    unsigned char size = (unsigned char)(1);

    /* act and assert */
    EXPECT_THROW(initialise_monitor(monitor, lines, size), std::out_of_range);
}

TEST_F(MonitorTestFixture, StartStop)
{
    /* arrange */
    const char* chip1 = "gpiochip0";
    unsigned char offsets1[2] = {(unsigned char)(6), (unsigned char)(7)};
    const char* chip2 = "gpiochip5";
    unsigned char offsets2[2] = {(unsigned char)(19), (unsigned char)(20)};
    chip_lines lines[2] = { {chip1, offsets1, (unsigned char)(2)},
                            {chip2, offsets2, (unsigned char)(2)} };
    unsigned char size = (unsigned char)(2);

    /* act */
    initialise_monitor(monitor, lines, size);

    std::promise<bool> _done;
    std::future<bool> result = _done.get_future();
    std::thread t([&](void* gpiodmon, std::promise<bool> done)
                  {
                      std::this_thread::sleep_for(std::chrono::seconds(1));
                      stop_monitor(gpiodmon);
                      done.set_value(true);
                  }, monitor, std::move(_done));
    start_monitor(monitor);

    /* assert */
    ASSERT_TRUE(result.get());
    t.join();
}

TEST_F(MonitorTestFixture, StartStopTwice)
{
    /* arrange */
    const char* chip = "gpiochip0";
    unsigned char offsets[2] = {(unsigned char)(6), (unsigned char)(7)};
    chip_lines lines[1] = { {chip, offsets, (unsigned char)(2)} };
    unsigned char size = (unsigned char)(1);

    /* act */
    initialise_monitor(monitor, lines, size);
    std::future<void> f1 = std::async(std::launch::async, [&](int msec)
    {
        std::this_thread::sleep_for(std::chrono::seconds(msec/1000));
        stop_monitor(monitor);
    }, 1000);
    start_monitor(monitor);
    std::future_status stat1 = f1.wait_for( std::chrono::milliseconds(1100));

    initialise_monitor(monitor, lines, size);
    std::future<void> f2 = std::async(std::launch::async, [&](int msec)
    {
        std::this_thread::sleep_for(std::chrono::seconds(msec/1000));
        stop_monitor(monitor);
    }, 1000);
    start_monitor(monitor);
    std::future_status stat2 = f2.wait_for( std::chrono::milliseconds(1100));

    /* assert */
    ASSERT_TRUE(stat1 == std::future_status::ready && stat2 == std::future_status::ready);
}

int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
