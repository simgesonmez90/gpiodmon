# GPIODMON LIBRARY

**gpiodmon** library has been basically developed using gpiod API to monitor input lines of gpio chips. It enables the users to listen multiple different gpiochip & offset pairs and keep the users updated of the latest input status. The number of maximum allowed users is 10. This project also includes **gpiodmon_test** which consists of a set of unit tests and c# / c++ test applications.
Please run **gpiodmon_test** on your platform and assure that all tests are passed before employing this library for your project.
c++ and c# demo applications have been written to example how to employ gpiodmon library in c++ and c# projects.

### Please cross-compile gpiod first by following the steps below, otherwise gpiodmon would fail to compile:
- Download libgpiod from https://git.kernel.org/pub/scm/libs/libgpiod/libgpiod.git/ (say libgpiod-1.6.3.tar.gz)
- Extract it to an empty folder named gpiod
- Change your directory to the root of lipgpiod folder ~/gpiod/libgpiod-1.6.3
- Run following commands:
    1. ac_cv_func_malloc_0_nonnull=yes ac_cv_func_realloc_0_nonnull=yes ./autogen.sh --enable-tool=yes --target=arm-linux-gnueabihf --prefix=/usr/arm-linux-gnueabihf/
    2. make
    3. make install

### Prerequisite to success of execution is to have gpiod installed on your platform. To install it, please run the commands below on your platform:
    1. apt-get install gpiod
    2. apt-get install libgpiod-dev

## To compile gpiodmon and gpiodmon_test
- Download gpiodmon project source code
- Create a toolchain.cmake file under project root and make system/compiler settings in it
- Create an output library named with **lib** under gpiodmon/gpiodmon/
- Change directory to ~/gpiodmon/gpiodmon/lib
- Run following commands with a proper toolchain.cmake file:
    1. cmake -DCMAKE_TOOLCHAIN_FILE=../../toolchain.cmake ../
    2. cmake --build . --target gpiodmon
    3. cmake --build . --target gpiodmon_test
- You will have both **gpiodmon** library and **gpiodmon_test** executable in "lib" directory.

## To compile c++ test application monitor_test
- Download gpiodmon project source code
- Create a toolchain.cmake file under project root and make system/compiler settings in it
- Create an output library named with "output" under gpiodmon/gpiodmon/
- Change directory to ~/gpiodmon/gpiodmon/output
- Run following commands with a proper toolchain.cmake file to compile c++ test-app:
    1. cmake -DCMAKE_TOOLCHAIN_FILE=../toolchain.cmake ../
    2. cmake --build . --target monitor_test 
- You will have a test application named with **monitor_test** which runs two different input monitors in parallel with periods of 15sec and 30sec. First monitor monitors DI1 whilst the second DI4.
In the case that input change happens, it is expected that **chip_name**, **offset** and **line** information on which the change occurs are printed out.
