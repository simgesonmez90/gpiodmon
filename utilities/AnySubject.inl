/**
 * @file AnySubject.inl
 * @brief AnySubject class implements any subject which any observer
 * listens for the events. AnyObserver is attached to the subject and
 * it gets updated by AnySubject about events.
 */
#ifndef __CROSS_UTILITIES_ANYSUBJECT_INL__
#define __CROSS_UTILITIES_ANYSUBJECT_INL__

#include <memory>
#include <vector>
#include <algorithm>
#include "AnyObserver.inl"

template<typename T, typename S>
class AnySubject
{
public:
    virtual ~AnySubject(){};
   /**
    * @brief Attach AnyObserver to the subject and add to the observer list.
    * @param observer AnyObserver to be attached and added to the list.
    */
    virtual void attach(std::shared_ptr<AnyObserver<T,S>> const& observer)
    {
        std::weak_ptr<AnyObserver<T,S>> obs_ptr = observer;
        auto found = std::find_if_not(m_observers.begin(), m_observers.end(),
                               [&obs_ptr](auto const& obs_in_list)
                               {
                                   return obs_ptr.lock() == obs_in_list.lock();
                               });
        if (found != m_observers.end())
        {
            return;
        }
        m_observers.emplace_back(obs_ptr);
    };

   /**
    * @brief Detach AnyObserver to the subject, if exists in the list.
    * @param observer AnyObserver to be detached and removed from the list.
    */
    virtual void detach(std::shared_ptr<AnyObserver<T,S>> const& observer)
    {
        if (m_observers.empty())
        {
            return;
        }
        std::weak_ptr<AnyObserver<T,S>> obs_ptr = observer;
        m_observers.erase(std::remove_if(m_observers.begin(), m_observers.end(),
                                 [&obs_ptr](auto const& obs_in_list)
                                 {
                                     return obs_ptr.lock() == obs_in_list.lock();
                                 }),m_observers.end());
    };

   /**
    * @brief Notify observers of the events.
    * @param subject object which update information belongs to.
    * @param info information updated by the events.
    */
    virtual void notify(T* subject, S& info) const
    {
        auto update_observers = [&](auto& observer)
                 {
                     observer.lock()->update(subject, info);
                 };

        std::for_each(m_observers.begin(), m_observers.end(), update_observers);
    };

private:
    std::vector<std::weak_ptr<AnyObserver<T,S>>> m_observers;
};
#endif /* __CROSS_UTILITIES_ANYSUBJECT_INL__ */