/**
 * @file AnyObserver.inl
 * @brief AnyObserver class implements any observer of any subject.
 * AnySubject notifies AnyObserver of the updates depending on events.
 */
#ifndef __CROSS_UTILITIES_ANYOBSERVER_INL__
#define __CROSS_UTILITIES_ANYOBSERVER_INL__

template<class T, typename S>
class AnyObserver
{
public:
    virtual ~AnyObserver(){};
   /**
    * @brief Notify observers about devices and information.
    * @param device object to which update information belongs.
    * @param info information of the device subject.
    */
    virtual void update(T* device, S& info) const = 0;
};
#endif /* __CROSS_UTILITIES_ANYOBSERVER_INL__ */